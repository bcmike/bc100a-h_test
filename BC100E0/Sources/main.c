// BC100A0  (git/bitbucket project)
// ======
// - test code to validate P100E0.pcb
// - see common.h for a description of all hardware signals; *** all references to specific Port Values can only be done in common.h ***
//
// Bugs:
//   - cannot play most MIDI files (MIDI streaming is OK)
//   - some white noise background hiss
//   - MP3 files with lots of extra info (e.g. album cover) take a lot of time to start: find a way to strip all these extras
//   [SOLVED] why are all "SCI" registers=0x00 after first lines of Test_MP3()? Only difference is GPIO1=0;
//            why does 0=0x4800, 1=0x0040 become 0=0x2400, 1=0x0020 after completing Test_MP3()?
//    ---> was exceeding chip default max SPI speed; this happened after a call to SD card (changed SPI)
//   [SOLVED] Seem to have a bad contact or bad ground
//            ---> enabled all outputs (as GPIOs for now) to SoundBoard and set them to 1
//   Change PIT timer delay to SYSTICK Timer

// INCLUDES /////////////////////////////////////////////////////////
//-------------------------------------------------------------------
#include "common.h"
#include "uart.h"
#include "spi.h"
#include "sd.h"
#include "vs1053.h"
#include "boardA_MCU_Sensors.h"
#include "boardB_Datalogger.h"
#include "boardD_LED_Driver.h"
#include "boardE_Sound_Microphone.h"
#include "boardF_CapTouch.h"
#include "boardG_InfraRed.h"


// DEFINES //////////////////////////////////////////////////////////
//-------------------------------------------------------------------



// GLOBAL CONSTANTS /////////////////////////////////////////////////
//-------------------------------------------------------------------


// GLOBAL VARIABLES /////////////////////////////////////////////////
//-------------------------------------------------------------------
byte brd;


// FUNCTION HEADERS /////////////////////////////////////////////////
//-------------------------------------------------------------------
void BoardA_MCU_Sensors(void);
void BoardB_Datalogger(void);
void BoardD_LED_Driver(void);
void BoardE_Sound_Microphone(void);
void BoardF_CapTouch(void);
void BoardG_InfraRed(void);


//**** MAIN *********************************************************
//===================================================================
int main(void)
{
MCU_Init();
PIT_Init();
RGB_Init();
LPUART0_SCG;
LPUART0_MUX;
DUART_Init(26);                                 // 48,000,000/16/115200 = 26

//SIM_SCGC5|=SIM_SCGC5_PORTC_MASK;              // used for clock gate C3 pin
//SIM_SOPT2=SIM_SOPT2_CLKOUTSEL(7);             // set IRC 48MHz
//PORTC_PCR3=PORT_PCR_MUX(5);                   // set CLKOUT to pin C3

//AUART_Init(96);                                   //DUART:115,200=terminal and AUART:31,250=midi
//SPI_Init(0x04); SD_Init(); VS_Init(); ADC_Init(); DAC_Init();
for (;;)
  {
	DUART_TextOut("\n\rP100E0 Validation v1.04: a=MCU_Sensors, b=Datalogger, d=LED_Driver, e=Sound_Microphone, f=CapTouch, g=InfraRed: ");
	brd=DUART_CharIn();
	switch (brd)
	  {
		//case 'a': BoardA_MCU_Sensors();      break;
		//case 'b': BoardB_Datalogger();       break;
		//case 'd': BoardD_LED_Driver();       break;
		case 'e': BoardE_Sound_Microphone(); break;
		//case 'f': BoardF_CapTouch();         break;
		//case 'g': BoardG_InfraRed();         break;
	  }
  }
return 0;
}//main
//===================================================================
//*******************************************************************



// FUNCTION BODIES //////////////////////////////////////////////////
//-------------------------------------------------------------------

