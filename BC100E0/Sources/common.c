// INCLUDES /////////////////////////////////////////////////////////                  
//-------------------------------------------------------------------
#include "common.h"


// FUNCTION BODIES //////////////////////////////////////////////////                    
//-------------------------------------------------------------------
void MCU_Init(void)
{
// Crucial
__disable_irq();                                                     // Disable interrupts
SIM_COPC=0x00;                                                       // Disable COP watchdog

MCG_MC|=MCG_MC_HIRCEN_MASK;                                          // Enable 48MHz HIRC
MCG_C1=0;                                                            // Enable Main Clock Source of the 48MHz HIRC

}
//-------------------------------------------------------------------
void PIT_Init(void)
{
SIM_SCGC6|=SIM_SCGC6_PIT_MASK;                                       // Enable PIT   module
PIT_MCR=0x00;                                                        // MDIS=0: enable timers
PIT_LDVAL0=0xFFFFFFFF;                                               // PIT0 use for measuring time intervals
PIT_LDVAL1=2400;                                                     // PIT1 Period=100us
PIT_TCTRL0=0x0001;                                                   // Enable PIT0, polling
PIT_TCTRL1=0x0001;                                                   // Enable PIT1, polling
}
//...................................................................
void PIT_Delay100us(dwrd delay)
{dwrd delw; for (delw=0;delw<delay;delw++) {while (!PIT_TFLG1); PIT_TFLG1=1;}}
//-------------------------------------------------------------------
void RGB_Init(void) {RGB_SCG; RGB_MUX; RGB_Outputs; RGB_Default;}
//...................................................................
void RGB(byte R,byte G,byte B)
{
if (R) RGB_R1; else RGB_R0;
if (G) RGB_G1; else RGB_G0;
if (B) RGB_B1; else RGB_B0;
}

// Errata 8068 fix
//SIM->SCGC6 |= SIM_SCGC6_RTC_MASK;            // enable clock to RTC
//RTC->TSR = 0x00;                             // dummy write to RTC TSR per errata 8068
//SIM->SCGC6 &= ~SIM_SCGC6_RTC_MASK;           // disable clock to RTC
