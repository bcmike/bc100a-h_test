// INCLUDES /////////////////////////////////////////////////////////                  
//-------------------------------------------------------------------
#include "uart.h"


// FUNCTION BODIES //////////////////////////////////////////////////                    
//-------------------------------------------------------------------
void LPUART0_Init(word bd)        {LPUART0_CTRL=0;  LPUART0_BAUD=LPUART_BAUD_SBR(bd); LPUART0_CTRL=(LPUART_CTRL_RE_MASK|LPUART_CTRL_TE_MASK);}
void LPUART0_CharOut(byte data)   {while ((LPUART0_STAT&LPUART_STAT_TDRE_MASK)==0); LPUART0_DATA=data;}
void LPUART0_NibbOut(byte data)   {if (data>0x09) LPUART0_CharOut(data+0x37); else LPUART0_CharOut(data+0x30);}
void LPUART0_ByteOut(byte data)   {LPUART0_NibbOut(data>>4); LPUART0_NibbOut(data&0x0F);}
void LPUART0_WordOut(word data)   {LPUART0_ByteOut(data>>8); LPUART0_ByteOut(data&0xFF);}
void LPUART0_DWrdOut(dwrd data)   {LPUART0_WordOut(data>>16); LPUART0_CharOut('_'); LPUART0_WordOut(data&0xFFFF);}
void LPUART0_TextOut(char *data)  {while (*data!=0x00) {LPUART0_CharOut(*data); data++;}}
byte LPUART0_RDRF(void)           {if ((LPUART0_STAT&LPUART_STAT_RDRF_MASK)==0) return 1; else return 0;}
byte LPUART0_CharIn(void)         {while ((LPUART0_STAT&LPUART_STAT_RDRF_MASK)==0); return ((LPUART0_DATA)&(0x000000FF));}
byte LPUART0_NibbIn(void)         {byte l; l=LPUART0_CharIn()-48; if (l>9) l-=7; return l&0x0F;}
byte LPUART0_ByteIn(void)         {byte h,l; h=LPUART0_CharIn()-48; if (h>9) h-=7; l=LPUART0_CharIn()-48; if (l>9) l-=7; return (h&15)*16+(l&15);}
void LPUART0_DbleOut(float data,byte d1,byte d2)
{
byte tmp,showzero,digs;
double idiv;
showzero=0;
idiv=1.0; for (digs=0;digs<d1;digs++) idiv*=10.0;
if (data<0) {LPUART0_CharOut('-'); data=-data;} else LPUART0_CharOut('+');
for (digs=0;digs<=d1+d2;digs++)
  {
  tmp=data/idiv;
  if (tmp) {LPUART0_NibbOut(tmp); showzero=1;} else if (showzero) LPUART0_NibbOut(tmp); else LPUART0_CharOut(' ');
  data-=tmp*idiv; idiv/=10.0;
  if (digs==d1) LPUART0_CharOut('.');
  }
}
//-------------------------------------------------------------------
void LPUART1_Init(word bd)        {LPUART1_CTRL=0;  LPUART1_BAUD=LPUART_BAUD_SBR(bd); LPUART1_CTRL=(LPUART_CTRL_RE_MASK|LPUART_CTRL_TE_MASK);}
void LPUART1_CharOut(byte data)   {while ((LPUART1_STAT&LPUART_STAT_TDRE_MASK)==0); LPUART1_DATA=data;}
void LPUART1_NibbOut(byte data)   {if (data>0x09) LPUART1_CharOut(data+0x37); else LPUART1_CharOut(data+0x30);}
void LPUART1_ByteOut(byte data)   {LPUART1_NibbOut(data>>4); LPUART1_NibbOut(data&0x0F);}
void LPUART1_WordOut(word data)   {LPUART1_ByteOut(data>>8); LPUART1_ByteOut(data&0xFF);}
void LPUART1_DWrdOut(dwrd data)   {LPUART1_WordOut(data>>16); LPUART1_CharOut('_'); LPUART1_WordOut(data&0xFFFF);}
void LPUART1_TextOut(char *data)  {while (*data!=0x00) {LPUART1_CharOut(*data); data++;}}
byte LPUART1_RDRF(void)           {if ((LPUART0_STAT&LPUART_STAT_RDRF_MASK)==0) return 1; else return 0;}
byte LPUART1_CharIn(void)         {while ((LPUART0_STAT&LPUART_STAT_RDRF_MASK)==0); return ((LPUART0_DATA)&(0x000000FF));}
byte LPUART1_NibbIn(void)         {byte l; l=LPUART1_CharIn()-48; if (l>9) l-=7; return l&0x0F;}
byte LPUART1_ByteIn(void)         {byte h,l; h=LPUART1_CharIn()-48; if (h>9) h-=7; l=LPUART1_CharIn()-48; if (l>9) l-=7; return (h&15)*16+(l&15);}
void LPUART1_DbleOut(float data,byte d1,byte d2)
{
byte tmp,showzero,digs;
double idiv;
showzero=0;
idiv=1.0; for (digs=0;digs<d1;digs++) idiv*=10.0;
if (data<0) {LPUART1_CharOut('-'); data=-data;} else LPUART1_CharOut('+');
for (digs=0;digs<=d1+d2;digs++)
  {
  tmp=data/idiv;
  if (tmp) {LPUART1_NibbOut(tmp); showzero=1;} else if (showzero) LPUART1_NibbOut(tmp); else LPUART1_CharOut(' ');
  data-=tmp*idiv; idiv/=10.0;
  if (digs==d1) LPUART1_CharOut('.');
  }
}
