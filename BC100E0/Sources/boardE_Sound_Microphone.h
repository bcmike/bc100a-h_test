#ifndef __BOARDE_SOUND_MICROPHONE_H
#define __BOARDE_SOUND_MICROPHONE_H


// INCLUDES /////////////////////////////////////////////////////////                 
//-------------------------------------------------------------------
#include "common.h"
#include "uart.h"
#include "vs1053.h"
#include "sd.h"
#include "math.h"


// DEFINES //////////////////////////////////////////////////////////                    
//-------------------------------------------------------------------
#define maxsongs       16
#define nsamples       8000


// GLOBAL VARIABLES /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
byte cmd,vsadr,bh,bl,songs,sderror,bvol;
byte m[nsamples];
word vsres;
dwrd dres,songlen[maxsongs],songentry[maxsongs];
extern byte SDout[512],SDin[512];


// FUNCTION HEADERS /////////////////////////////////////////////////                    
//-------------------------------------------------------------------
void BoardE_Sound_Microphone(void);
//...................................................................
void Test_MIDI_realtime(void);
void Test_Music_files(void);
void Test_Microphone(void);
void Test_DAC(void);
//...................................................................
void MIDI_NoteOn(byte note,byte vol);
void MIDI_NoteOff(byte note);
byte CheckForVolumeChange(void);
void SD_PlayFile(byte f);
void SD_Analyze(void);
void ADC_Init(void);
word ADC_Read(int chan);
void DAC_Init(void);


#endif //__BOARDE_SOUND_MICROPHONE_H
