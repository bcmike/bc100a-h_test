// INCLUDES /////////////////////////////////////////////////////////                  
//-------------------------------------------------------------------
#include "vs1053.h"


// FUNCTION BODIES //////////////////////////////////////////////////                    
//-------------------------------------------------------------------
void VS_Init(void)
{
	    SPI_VS_CS_SCG;      SPI_VS_CS_MUX;      SPI_VS_CS_Outputs;      SPI_VS_CS_Default;
     SPI_VS_DCS_SCG;     SPI_VS_DCS_MUX;     SPI_VS_DCS_Outputs;     SPI_VS_DCS_Default;
 VS1053_MP3MIDI_SCG; VS1053_MP3MIDI_MUX; VS1053_MP3MIDI_Outputs; VS1053_MP3MIDI_Default;
      VS1053_On_SCG;      VS1053_On_MUX;      VS1053_On_Outputs;      VS1053_On_Default;
    VS1053_DREQ_SCG;    VS1053_DREQ_MUX;
}
//-------------------------------------------------------------------
void VS_FullReset(void)
{
VS1053_Off_Reset; PIT_Delay100us(100); VS1053_On;          // *** CAUTION: VS1053 wakes up in low SPI mode ***
VS_RegWrite(0x0B,0x0505);                                  // stereo volume attenuation: 0x0000=max volume, 0xFEFE=min volume
VS_RegWrite(0x03,0x6000);                                  // increase clock multiplier to 3x
DUART_TextOut("\n\rVS Reset!");
}
//-------------------------------------------------------------------
void VS_Status(void)
{
word wres;
wres=VS_RegRead(0x00); DUART_TextOut("\n\rVS Mode  =0x"); DUART_WordOut(wres);
wres=VS_RegRead(0x01); DUART_TextOut("\n\rVS Status=0x"); DUART_WordOut(wres);
wres=VS_RegRead(0x03); DUART_TextOut("\n\rVS ClockF=0x"); DUART_WordOut(wres);
}
//-------------------------------------------------------------------
byte VS_Free(char c,byte debug)
{
byte bres;
bres=VS1053_DREQ;                                          // DREQ=1 when VS available; DREQ goes to 0 when busy
if ((debug==1)&&(bres==0)) DUART_CharOut(c);
return bres;
}
//-------------------------------------------------------------------
void VS_RegWrite(byte adr,word val)
{
while (!VS_Free('!',1));
SPI_VS_CS0;
SPI_CharShift(0x02); SPI_CharShift(adr); SPI_CharShift(val>>8); SPI_CharShift(val&0xFF);
while (!VS_Free('@',1));
SPI_VS_CS1;
}
//-------------------------------------------------------------------
word VS_RegRead(byte adr)
{
byte h,l;
while (!VS_Free('#',1));
SPI_VS_CS0;
SPI_CharShift(0x03); SPI_CharShift(adr);
h=SPI_CharShift(l); while (!VS_Free('$',1));
l=SPI_CharShift(l); while (!VS_Free('%',1));
SPI_VS_CS1;
return h*256+l;
}
//-------------------------------------------------------------------
void VS_Play32B(word index)
{
byte i;
while (!VS_Free('^',0));
SPI_VS_DCS0;
for (i=0;i<32;i++) SPI_CharShift(SDin[index+i]);
SPI_VS_DCS1;
}
