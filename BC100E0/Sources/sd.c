// INCLUDES /////////////////////////////////////////////////////////                  
//-------------------------------------------------------------------
#include "sd.h"


// FUNCTION BODIES //////////////////////////////////////////////////                    
//-------------------------------------------------------------------
void SD_Init(void) {SPI_SD_SS_SCG; SPI_SD_SS_MUX; SPI_SD_SS_Outputs; SPI_SD_SS_Default;}
//-------------------------------------------------------------------
void SD_ClockCmd(byte cmd,byte a0,byte a1,byte a2,byte a3,byte crc)
{
(void)SPI_CharShift(cmd|0x40);
(void)SPI_CharShift(a0); (void)SPI_CharShift(a1); (void)SPI_CharShift(a2); (void)SPI_CharShift(a3);
(void)SPI_CharShift(crc);
}
//-------------------------------------------------------------------
byte SD_SendCmd(byte cmd,byte a0,byte a1,byte a2,byte a3,byte crc)
{
byte ilim,cin,rlen;
switch (cmd)
  {
  case  0: rlen=1; break; //  CMD0  --> R1
  case  8: rlen=5; break; //  CMD8  --> R7
  case 41: rlen=1; break; // ACDM41 --> R1
  case 55: rlen=1; break; //  CMD55 --> R1
  case 58: rlen=5; break; //  CMD58 --> R3
  default: rlen=1; break;
  }
SPI_SD_SS0;
(void)SD_ClockCmd(cmd,a0,a1,a2,a3,crc);
ilim=0; do {cin=SPI_CharShift(0xFF); ilim++;} while ((ilim<5)&&(cin&0x80));
if (ilim>=5) {SPI_SD_SS1; return 99;}
SDin[0]=cin;
for (ilim=0;ilim<rlen;ilim++) {cin=SPI_CharShift(0xFF); if (cin!=0xFF) SDin[ilim+1]=cin;}
SPI_SD_SS1;
return 0;
}
//-------------------------------------------------------------------
byte SD_CardReset(void)
{
byte senderror;
word iw;
SPI0_BR=0x05;                                              // SPI=bus/64 (375kbps)
SPI_SD_SS1;
for (iw=0;iw<10;iw++) (void)SPI_CharShift(0xFF); DUART_TextOut("  0. Card 'reset' to accept Native Commands.\n\r");
iw=0; do {senderror=SD_SendCmd(0,0,0,0,0,0x95); iw++;} while ((iw<5)&&((SDin[0]!=0x01)||(senderror)));
if (iw>=5) {DUART_TextOut("  ERROR: Card could not be moved into Idle State. ---> Do you have a card in the slot? <---\n\r"); return 99;}
DUART_TextOut("  1. Card moved into Idle State.\n\r");
senderror=SD_SendCmd(8,0,0,0x01,0xAA,0x87);
if ((SDin[0]!=0x01)||(SDin[1]!=0x00)||(SDin[2]!=0x00)||(SDin[3]!=0x01)||(SDin[4]!=0xAA)||(senderror))
  {DUART_TextOut("  ERROR: Card is not a valid SD v2.\n\r"); return 99;}
DUART_TextOut("  2. Valid SD Card: rev2.x\n\r");
DUART_TextOut("  3. Trying to initialize card: ");
iw=0; do {senderror=SD_SendCmd(55,0,0,0,0,0x95); senderror=SD_SendCmd(41,0x40,0,0,0,0x95); DUART_CharOut('.'); iw++;} while ((iw<500)&&(SDin[0]));
if (iw>=500) {DUART_TextOut("\n\r  ERROR: Card could not be initialized.\n\r"); return 99;}
DUART_TextOut("\n\r");
senderror=SD_SendCmd(58,0,0,0,0,0x95);
if ((SDin[0]!=0x00)||(SDin[1]!=0xC0)||(senderror))
  {DUART_TextOut("  ERROR: Could not read Card Operating Conditions.\n\r"); return 99;}
DUART_TextOut("  4. Card Operating Conditions: 2.7V-3.6V, 512B block size.\n\r");
SPI0_BR=0x00;                                              // SPI=bus/2 (12Mbps)
return 0;
}
//-------------------------------------------------------------------
byte SD_WriteBlock(dwrd sdadr)
{
byte cin;
word iw;
SPI_SD_SS0;
SD_ClockCmd(24,sdadr>>24,(sdadr>>16)&0xFF,(sdadr>>8)&0xFF,sdadr&0xFF,0x95);
iw=0; do {cin=SPI_CharShift(0xFF); iw++;} while ((iw<10)&&(cin&0x80));
if ((iw>=10)||(cin)) {DUART_TextOut("!!! ERROR: no response during SD_WriteBlock()\n\r"); SPI_SD_SS1; return 99;}
cin=SPI_CharShift(0xFF);                                   // 1 byte space required
cin=SPI_CharShift(0xFE);                                   // data token
for (iw=0;iw<512;iw++) cin=SPI_CharShift(SDout[iw]);       // clock out entire 512B
cin=SPI_CharShift(0xAB);                                   // dummy CRC-hi
cin=SPI_CharShift(0xCD);                                   // dummy CRC-lo
// Read Response from card
cin=SPI_CharShift(0xFF);
if ((cin&0x0F)!=0x05) {DUART_TextOut("!!! ERROR: bad ack during SD_WriteBlock()\n\r"); SPI_SD_SS1; return 99;}
// Wait until Write is complete
iw=0; do {cin=SPI_CharShift(0xFF); iw++;} while ((iw<50000)&&(cin!=0xFF));
if (iw>=50000) {DUART_TextOut("!!! ERROR: busy status too long during SD_WriteBlock()\n\r"); SPI_SD_SS1; return 99;}
(void)SPI_CharShift(0xFF);
SPI_SD_SS1;
return 0;
}
//-------------------------------------------------------------------
byte SD_ReadBlock(dwrd sdadr)
{
byte cin,mask;
word iw,truecrc;
dwrd crc16;
SPI_SD_SS0;
SD_ClockCmd(17,sdadr>>24,(sdadr>>16)&0xFF,(sdadr>>8)&0xFF,sdadr&0xFF,0x95);
iw=0; do {cin=SPI_CharShift(0xFF); iw++;} while ((iw<10)&&(cin&0x80));
if ((iw>=10)||(cin)) {DUART_TextOut("!!! ERROR: no response during SD_ReadBlock()\n\r"); SPI_SD_SS1; return 99;}
iw=0; do {cin=SPI_CharShift(0xFF); iw++;} while ((iw<5000)&&(cin!=0xFE));
if (iw>=5000) {DUART_TextOut("!!! ERROR: busy status too long during SD_ReadBlock()\n\r"); SPI_SD_SS1; return 99;}
// Read block
for (iw=0;iw<512;iw++) {cin=SPI_CharShift(0xFF); SDin[iw]=cin;}
truecrc=SPI_CharShift(0xFF)<<8|SPI_CharShift(0xFF);
for (iw=0;iw<10;iw++) cin=SPI_CharShift(0xFF); // RODMOD why is this needed? Do we need to flush after the CRC?
SPI_SD_SS1;
crc16=0x0000;
for (iw=0;iw<512;iw++)
  {
  cin=SDin[iw];
  mask=0x80;
  while (mask)
    {
    crc16<<=1;
    if (cin&mask) crc16|=1;
    if (crc16&0x10000) {crc16=crc16&0xFFFF; crc16=crc16^0x1021;}
    mask>>=1;
    }
  }
for (iw=0;iw<16;iw++) {crc16<<=1; if (crc16&0x10000) {crc16=crc16&0xFFFF; crc16=crc16^0x1021;}}
if (crc16==truecrc) return 0; else return 99;
}
