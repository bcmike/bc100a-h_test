#ifndef __VS1053_H
#define __VS1053_H


// INCLUDES /////////////////////////////////////////////////////////                 
//-------------------------------------------------------------------
#include "common.h"                                                  // provides SPI_VS_CS0,SPI_VS_CS1,SPI_VS_DCS0,SPI_VS_DCS1,VS1053_Off_Reset,VS1053_On macros
#include "spi.h"                                                     // requires access to the SPI
#include "uart.h"                                                    // provides debug on the terminal


// DEFINES //////////////////////////////////////////////////////////                    
//-------------------------------------------------------------------


// GLOBAL VARIABLES /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
extern byte SDout[512],SDin[512];


// FUNCTION HEADERS /////////////////////////////////////////////////                    
//-------------------------------------------------------------------  
void VS_Init(void);
void VS_FullReset(void);
void VS_Status(void);
byte VS_Free(char c,byte debug);
void VS_RegWrite(byte adr,word val);
word VS_RegRead(byte adr);
void VS_Play32B(word index);


#endif //__VS1053_H

