#ifndef __UART_H
#define __UART_H


// INCLUDES /////////////////////////////////////////////////////////                 
//-------------------------------------------------------------------
#include "common.h"


// DEFINES //////////////////////////////////////////////////////////                    
//-------------------------------------------------------------------
#define RDRF                      0x20                               // for UART calls
#define TDRE                      0x80                               // for UART calls


// GLOBAL VARIABLES /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    


// FUNCTION HEADERS /////////////////////////////////////////////////                    
//-------------------------------------------------------------------  
void LPUART0_Init(word data);                                          // initializes the serial port
void LPUART0_CharOut(byte data);                                       // sends out a character
void LPUART0_NibbOut(byte data);                                       // sends out a nibble (hex value)
void LPUART0_ByteOut(byte data);                                       // sends out a byte (hex value)
void LPUART0_WordOut(word data);                                       // sends out a word (hex value)
void LPUART0_DWrdOut(dwrd data);                                       // sends out a double word (hex value)
void LPUART0_TextOut(char *data);                                      // sends out a string
byte LPUART0_RDRF(void);                                               // checks to see if a character is present
byte LPUART0_CharIn(void);                                             // reads a character
byte LPUART0_NibbIn(void);
byte LPUART0_ByteIn(void);                                             // reads two characters and interprets them as a byte (hex)
void LPUART0_DbleOut(float data,byte d1,byte d2);                      // sends out double (float); d1=digits before decimal point, d2=after
//-------------------------------------------------------------------
void LPUART1_Init(word data);                                          // initializes the serial port
void LPUART1_CharOut(byte data);                                       // sends out a character
void LPUART1_NibbOut(byte data);                                       // sends out a nibble (hex value)
void LPUART1_ByteOut(byte data);                                       // sends out a byte (hex value)
void LPUART1_WordOut(word data);                                       // sends out a word (hex value)
void LPUART1_DWrdOut(dwrd data);                                       // sends out a double word (hex value)
void LPUART1_TextOut(char *data);                                      // sends out a string
byte LPUART1_RDRF(void);                                               // checks to see if a character is present
byte LPUART1_CharIn(void);                                             // reads a character
byte LPUART1_NibbIn(void);
byte LPUART1_ByteIn(void);                                             // reads two characters and interprets them as a byte (hex)
void LPUART1_DbleOut(float data,byte d1,byte d2);                      // sends out double (float); d1=digits before decimal point, d2=after


#endif //__UART_H
