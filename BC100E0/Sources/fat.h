#ifndef __FAT_H
#define __FAT_H


// INCLUDES /////////////////////////////////////////////////////////                 
//-------------------------------------------------------------------
#include "common.h"                                                  // provides SPI_SD_SS0 and SPI_SD_SS1 macros
#include "spi.h"                                                     // requires access to the SPI
#include "uart.h"                                                    // provides debug on the terminal


// DEFINES //////////////////////////////////////////////////////////                    
//-------------------------------------------------------------------
#define fileadr        0x4328                                        // see SDcard_Progression.txt


// GLOBAL VARIABLES /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
extern dwrd pout;


// FUNCTION HEADERS /////////////////////////////////////////////////                    
//-------------------------------------------------------------------
void FAT_Analyze(void);
/*
byte FAT_SetFSize(dwrd data);
byte FAT_TryWritingBlock(dwrd data);
byte FAT_Format(void);
void FAT_CharOut(byte data);
void FAT_Chr2Out(word data);
void FAT_Chr4Out(dwrd data);
void FAT_Chr4OutR(dwrd data);
void FAT_NibbOut(byte data);
void FAT_ByteOut(byte data);
void FAT_WordOut(word data);
void FAT_DWrdOut(dwrd data);
void FAT_TextOut(char *data);
void FAT_ClrBuf(void);
void FAT_DecOut(dwrd data,byte digits);
*/


#endif //__FAT_H
