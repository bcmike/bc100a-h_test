#ifndef __COMMON_H
#define __COMMON_H


// HARDWARE /////////////////////////////////////////////////////////
//-------------------------------------------------------------------
//   A01: LPUART0_RX  = not used
//   A02: LPUART0_TX  = AUART_TX                 (sends MIDI realtime data to VS1053)
//   B00: GPIO_O      = VS1053_DCS
//   B01: GPIO_I      = VS1053_DREQ
//   B18: GPIO_O      = nRGB_R
//   B19: GPIO_O      = nRGB_G
//   C00: GPIO_O      = VS1053_nMP3/MIDI
//   C01: GPIO_O      = VS1053_CS
//   C06: GPIO_O      = VS1053_nRST
//   D00: GPIO_O      = SD_nSS
//   D01: SPI SCK     = SD and VS1053 SCK        (nRGB_B not used due to conflict)
//   D02: SPI MOSI    = SD and VS1053 MOSI
//   D03: SPI MISO    = SD and VS1053 MISO
//   E00: LPUART1_TX  = DUART_TX
//   E01: LPUART1_RX  = DUART_RX


// INCLUDES /////////////////////////////////////////////////////////                 
//-------------------------------------------------------------------
#include "MKL27Z4.h"
//#include "system_MKL27Z4.h"


// TYPEDEFS /////////////////////////////////////////////////////////
//-------------------------------------------------------------------
typedef unsigned char       byte;
typedef unsigned short int  word;
typedef unsigned long int   dwrd;
typedef char			    int8;
typedef short int	        int16;
typedef int		            int32;


// DEFINES //////////////////////////////////////////////////////////                    
//-------------------------------------------------------------------
#define RGB_SCG                   SIM_SCGC5|=SIM_SCGC5_PORTB_MASK;
#define RGB_MUX                   PORTB_PCR17=PORT_PCR_MUX(1); PORTB_PCR16=PORT_PCR_MUX(1); PORTB_PCR3 =PORT_PCR_MUX(1)
#define RGB_Outputs               GPIOB_PDDR|=(1<<17); GPIOB_PDDR|=(1<<16); GPIOB_PDDR|=(1<< 3)
#define RGB_Default               RGB(0,0,0)
#define RGB_R0                    GPIOB_PSOR=(1<<17)
#define RGB_R1                    GPIOB_PCOR=(1<<17)
#define RGB_G0                    GPIOB_PSOR=(1<<16)
#define RGB_G1                    GPIOB_PCOR=(1<<16)
#define RGB_B0                    GPIOB_PSOR=(1<< 3)
#define RGB_B1                    GPIOB_PCOR=(1<< 3)
//...................................................................
#define SPI_SCG                   SIM_SCGC4|=SIM_SCGC4_SPI0_MASK; SIM_SCGC5|=SIM_SCGC5_PORTD_MASK
#define SPI_MUX                   PORTD_PCR1 =PORT_PCR_MUX(2); PORTD_PCR2 =PORT_PCR_MUX(2); PORTD_PCR3 =PORT_PCR_MUX(2)
//...................................................................
#define SPI_SD_SS_SCG             SIM_SCGC5|=SIM_SCGC5_PORTD_MASK
#define SPI_SD_SS_MUX             PORTD_PCR0 =PORT_PCR_MUX(1)
#define SPI_SD_SS_Outputs         GPIOD_PDDR|=(1<< 0)
#define SPI_SD_SS_Default         GPIOD_PDOR|=(1<< 0)
#define SPI_SD_SS0                GPIOD_PCOR =(1<< 0)
#define SPI_SD_SS1                GPIOD_PSOR =(1<< 0)
//...................................................................
#define SPI_VS_CS_SCG             SIM_SCGC5|=SIM_SCGC5_PORTC_MASK
#define SPI_VS_CS_MUX             PORTC_PCR1 =PORT_PCR_MUX(1)
#define SPI_VS_CS_Outputs         GPIOC_PDDR|=(1<< 1)
#define SPI_VS_CS_Default         GPIOC_PDOR|=(1<< 1)
#define SPI_VS_CS0                GPIOC_PCOR =(1<< 1)
#define SPI_VS_CS1                GPIOC_PSOR =(1<< 1)
//...................................................................
#define SPI_VS_DCS_SCG            SIM_SCGC5|=SIM_SCGC5_PORTB_MASK
#define SPI_VS_DCS_MUX            PORTB_PCR0 =PORT_PCR_MUX(1)
#define SPI_VS_DCS_Outputs        GPIOB_PDDR|=(1<< 0)
#define SPI_VS_DCS_Default        GPIOB_PDOR|=(1<< 0)
#define SPI_VS_DCS0               GPIOB_PCOR =(1<< 0)
#define SPI_VS_DCS1               GPIOB_PSOR =(1<< 0)
//...................................................................
#define VS1053_MP3MIDI_SCG        SIM_SCGC5|=SIM_SCGC5_PORTC_MASK
#define VS1053_MP3MIDI_MUX        PORTC_PCR0 =PORT_PCR_MUX(1)
#define VS1053_MP3MIDI_Outputs    GPIOC_PDDR|=(1<< 0)
#define VS1053_MP3MIDI_Default    GPIOC_PDOR|=(1<< 0)
#define VS1053_MP3mode            GPIOC_PCOR =(1<< 0)
#define VS1053_MIDImode           GPIOC_PSOR =(1<< 0)
//...................................................................
#define VS1053_On_SCG             SIM_SCGC5|=SIM_SCGC5_PORTC_MASK
#define VS1053_On_MUX             PORTC_PCR6 =PORT_PCR_MUX(1)
#define VS1053_On_Outputs         GPIOC_PDDR|=(1<< 6)
#define VS1053_On_Default         GPIOC_PDOR|=(1<< 6)
#define VS1053_On                 GPIOC_PSOR =(1<< 6)
#define VS1053_Off_Reset          GPIOC_PCOR =(1<< 6)
//...................................................................
#define VS1053_DREQ_SCG           SIM_SCGC5|=SIM_SCGC5_PORTB_MASK
#define VS1053_DREQ_MUX           PORTB_PCR1 =PORT_PCR_MUX(1)
#define VS1053_DREQ               GPIOB_PDIR&(1<< 1)
//...................................................................
#define DUART_Init                LPUART0_Init
#define DUART_CharOut             LPUART0_CharOut
#define DUART_NibbOut             LPUART0_NibbOut
#define DUART_ByteOut             LPUART0_ByteOut
#define DUART_WordOut             LPUART0_WordOut
#define DUART_DWrdOut             LPUART0_DWrdOut
#define DUART_TextOut             LPUART0_TextOut
#define DUART_RDRF                LPUART0_RDRF
#define DUART_CharIn              LPUART0_CharIn
#define DUART_NibbIn              LPUART0_NibbIn
#define DUART_ByteIn              LPUART0_ByteIn
#define DUART_DbleOut             LPUART0_DbleOut
//...................................................................
#define AUART_Init                LPUART1_Init
#define AUART_CharOut             LPUART1_CharOut
#define AUART_NibbOut             LPUART1_NibbOut
#define AUART_ByteOut             LPUART1_ByteOut
//...................................................................
#define LPUART0_SCG               SIM_SCGC5|=SIM_SCGC5_LPUART0_MASK; SIM_SCGC5|=SIM_SCGC5_PORTA_MASK; SIM_SOPT2|=SIM_SOPT2_LPUART0SRC(1);
#define LPUART0_MUX               PORTA_PCR1 =PORT_PCR_MUX(2); PORTA_PCR2 =PORT_PCR_MUX(2)
//...................................................................
#define LPUART1_SCG               SIM_SCGC5|=SIM_SCGC5_LPUART1_MASK; SIM_SCGC5|=SIM_SCGC5_PORTE_MASK; SIM_SOPT2|=SIM_SOPT2_LPUART1SRC(1);
#define LPUART1_MUX               PORTE_PCR0 =PORT_PCR_MUX(3); PORTE_PCR1 =PORT_PCR_MUX(3)


// GLOBAL VARIABLES /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    


// FUNCTION HEADERS /////////////////////////////////////////////////                    
//-------------------------------------------------------------------
void MCU_Init(void);
//...................................................................
void PIT_Init(void);
void PIT_Delay100us(dwrd delay);
//...................................................................
void RGB_Init(void);
void RGB(byte R,byte G,byte B);


#endif //__COMMON_H
