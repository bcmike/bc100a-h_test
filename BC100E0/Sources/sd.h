#ifndef __SD_H
#define __SD_H


// INCLUDES /////////////////////////////////////////////////////////                 
//-------------------------------------------------------------------
#include "common.h"                                                  // provides SPI_SD_SS0 and SPI_SD_SS1 macros
#include "spi.h"                                                     // requires access to the SPI
#include "uart.h"                                                    // provides debug on the terminal


// DEFINES //////////////////////////////////////////////////////////                    
//-------------------------------------------------------------------


// GLOBAL VARIABLES /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    
byte SDout[512],SDin[512];


// FUNCTION HEADERS /////////////////////////////////////////////////                    
//-------------------------------------------------------------------
void SD_Init(void);                                                  // set up SPI nSS
void SD_ClockCmd(byte cmd,byte a0,byte a1,byte a2,byte a3,byte crc); // clocks out 6 bytes on SPI
byte SD_SendCmd(byte cmd,byte a0,byte a1,byte a2,byte a3,byte crc);  // SPI_SS0() + SD_ClockCmd + SPI_SS1()
byte SD_CardReset(void);                                             // get SD card ready to be programmed
byte SD_WriteBlock(dwrd sdadr);                                      // writes 512 bytes to   block sdadr
byte SD_ReadBlock(dwrd sdadr);                                       // reads  512 bytes from block sdadr


#endif //__SD_H
