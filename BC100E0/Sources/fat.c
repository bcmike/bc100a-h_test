// INCLUDES /////////////////////////////////////////////////////////                  
//-------------------------------------------------------------------
#include "fat.h"


// FUNCTION BODIES //////////////////////////////////////////////////                    
//-------------------------------------------------------------------
void FAT_Analyze(void)
{

}
//-------------------------------------------------------------------
/*
byte FAT_SetFSize(dwrd data)
{
FAT_ClrBuf(); 
   // Volume Label
FAT_TextOut("BC11logs   "); FAT_CharOut(0x08); FAT_Chr4Out(0x0000F95C); FAT_Chr4Out(0x7E437E43); FAT_Chr4Out(0x0000F95C); FAT_Chr4Out(0x7E430000); FAT_Chr4Out(0x00000000);
   // Not sure what this is
FAT_Chr4Out(0x41640061); FAT_Chr4Out(0x00740061); FAT_Chr4Out(0x006C000F); FAT_Chr4Out(0x002C6F00); FAT_Chr4Out(0x67002E00); FAT_Chr4Out(0x74007800); 
FAT_Chr4Out(0x74000000); FAT_Chr4Out(0x0000FFFF);
   // Log File
FAT_TextOut("DATALOG TXT"); FAT_CharOut(0x20); FAT_Chr4Out(0x0064095D); FAT_Chr4Out(0x7E437E43); FAT_Chr4Out(0x0000095D); FAT_Chr4Out(0x7E430300); 
   // File Size (little endian)
FAT_Chr4OutR(data);
return 1;
}
//-------------------------------------------------------------------
byte FAT_TryWritingBlock(dwrd tryadr)
{
byte tries,writeerror;
tries=0; do {writeerror=SD_WriteBlock(tryadr); if (writeerror) UART1_CharOut('?'); tries++;} while ((tries<10)&&(writeerror));
if (tries>=10) {UART1_TextOut(" fail! "); return 99;}
return 0;
}
//-------------------------------------------------------------------
byte FAT_Format(void)
{
// Create Clean MBR
FAT_ClrBuf();
pout=0x01B8; FAT_Chr4Out(0xAABBCCDD); pout=0x01BF; FAT_Chr4Out(0x2103000C); FAT_Chr4Out(0x69DEFF00); FAT_Chr4Out(0x08000000); FAT_Chr4Out(0x24760000);
pout=0x01FE; FAT_Chr2Out(0x55AA);
UART1_TextOut("  A) Creating MBR..."); if (FAT_TryWritingBlock(0x0000)) return 99;
// Create Boot Sector 1 of 2
FAT_ClrBuf();
FAT_Chr4Out(0xEB58906D); FAT_Chr4Out(0x6B646F73); FAT_Chr4Out(0x66730000); FAT_Chr4Out(0x02082000); 
FAT_Chr4Out(0x02000000); FAT_Chr4Out(0x00F80000); FAT_Chr4Out(0x3E007A00); FAT_Chr4Out(0x00000000);
FAT_Chr4Out(0x00247600); FAT_Chr4Out(0x801D0000); FAT_Chr4Out(0x00000000); FAT_Chr4Out(0x02000000);
FAT_Chr4Out(0x01000600); pout=0x0042;            FAT_Chr4Out(0x293EB878); FAT_CharOut(0x2E); FAT_TextOut("BC11logs   "); FAT_TextOut("FAT32   ");
FAT_Chr4Out(0x0E1FBE77); FAT_Chr4Out(0x7CAC22C0); FAT_Chr4Out(0x740B56B4); FAT_Chr4Out(0x0EBB0700);
FAT_Chr4Out(0xCD105EEB); FAT_Chr4Out(0xF032E4CD); FAT_Chr4Out(0x16CD19EB); FAT_CharOut(0xFE);
FAT_TextOut("This is not a bootable disk.  Please insert a bootable floppy and\n\rpress any key to try again ... \n\a");
pout=0x1FC; FAT_Chr4Out(0x000055AA);
UART1_TextOut("\n\r  B) Creating Boot Sector 1 of 2..."); if (FAT_TryWritingBlock(0x0800)) return 99;
// Create Boot Sector 2 of 2 (reuse data)
UART1_TextOut("\n\r  C) Creating Boot Sector 2 of 2..."); if (FAT_TryWritingBlock(0x0806)) return 99;
// Create FS Info Sector
FAT_ClrBuf();
FAT_TextOut("RRaA"); pout=0x01E4; FAT_TextOut("rrAa"); FAT_Chr4Out(0x1ABD0E00); FAT_Chr4Out(0x03000000); pout=0x01FC; FAT_Chr4Out(0x0000055AA); 
UART1_TextOut("\n\r  D) Creating FS Info Sector..."); if (FAT_TryWritingBlock(0x0801)) return 99;
// Create FAT 1 of 2
FAT_ClrBuf(); FAT_Chr4Out(0xF8FFFF0F); FAT_Chr4Out(0xFFFFFF0F); FAT_Chr4Out(0xF8FFFF0F); 
UART1_TextOut("\n\r  E) Creating FAT 1 of 2..."); if (FAT_TryWritingBlock(0x0820)) return 99;
// Create FAT 2 of 2 (reuse data)
UART1_TextOut("\n\r  F) Creating FAT 2 of 2..."); if (FAT_TryWritingBlock(0x25A0)) return 99;
// Create Directory Table
(void)FAT_SetFSize(22);
UART1_TextOut("\n\r  G) Creating Directory Table..."); if (FAT_TryWritingBlock(0x4320)) return 99;
// Min File Content
FAT_ClrBuf(); FAT_TextOut("0123456789\nABCDEFGHIJ\n");
UART1_TextOut("\n\r  H) Creating File..."); if (FAT_TryWritingBlock(fileadr)) return 99;
return 0;
}
//-------------------------------------------------------------------
void FAT_CharOut(byte data)   {SDout[pout++]=data;}
void FAT_Chr2Out(word data)   {FAT_CharOut(data>>8); FAT_CharOut(data&0xFF);}
void FAT_Chr4Out(dwrd data)   {FAT_Chr2Out(data>>16); FAT_Chr2Out(data&0xFFFF);}
void FAT_Chr4OutR(dwrd data)  {FAT_CharOut(data&0xFF); FAT_CharOut((data>>8)&0xFF); FAT_CharOut((data>>16)&0xFF); FAT_CharOut(data>>24);}
void FAT_NibbOut(byte data)   {if (data>0x09) FAT_CharOut(data+0x37); else FAT_CharOut(data+0x30);}
void FAT_ByteOut(byte data)   {FAT_NibbOut(data>>4); FAT_NibbOut(data&0x0F);}
void FAT_WordOut(word data)   {FAT_ByteOut(data>>8); FAT_ByteOut(data&0xFF);}
void FAT_DWrdOut(dwrd data)   {FAT_WordOut(data>>16); FAT_WordOut(data&0xFFFF);}
void FAT_TextOut(char *data)  {while (*data!=0x00) {FAT_CharOut(*data); data++;}}
void FAT_ClrBuf(void)         {word clrbuf; for (clrbuf=0;clrbuf<512;clrbuf++) SDout[clrbuf]=0; pout=0;}
//...................................................................
void FAT_DecOut(dwrd data,byte digits)
{
byte dig,i,notfirst;
dwrd ten;
ten=1;
for (i=0;i<digits-1;i++) ten*=10;
notfirst=0;
for (i=0;i<digits;i++)
  {
  dig=data/ten; 
  if ((dig==0)&&(notfirst==0)&&(i!=digits-1)) FAT_CharOut(' ');
  else {FAT_CharOut('0'+dig); notfirst=1;}
  data-=ten*dig; ten/=10;
  }
}
*/
















