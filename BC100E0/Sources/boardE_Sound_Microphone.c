// INCLUDES /////////////////////////////////////////////////////////                  
//-------------------------------------------------------------------
#include "boardE_Sound_Microphone.h"


// FUNCTION BODIES //////////////////////////////////////////////////                    
//-------------------------------------------------------------------
void BoardE_Sound_Microphone(void)
{
songs=0; sderror=1;
DUART_TextOut("\n\r  1=MIDI_realtime, 2=Music_files, 3=Microphone, 4=DAC, 5=VS1053_ReadWrite: "); //(r)ead_VS, (w)rite_VS, (i)nit_VS, (s)tatus_VS: ");
cmd=DUART_CharIn();
switch (cmd)
  {
	case '1': Test_MIDI_realtime(); break;
	case '2': SD_Analyze(); Test_Music_files(); break;
	case '3': Test_Microphone(); break;
	case '4': Test_DAC(); break;
/*
	case 'r':
		DUART_TextOut("Read  VS address:"); vsadr=DUART_NibbIn(); DUART_NibbOut(vsadr);
		DUART_CharOut('='); vsres=VS_RegRead(vsadr); DUART_WordOut(vsres);
		break;
	case 'w':
		DUART_TextOut("Write VS address:"); vsadr=DUART_NibbIn(); DUART_NibbOut(vsadr);
		DUART_CharOut(':'); bh=DUART_ByteIn(); DUART_ByteOut(bh); bl=DUART_ByteIn(); DUART_ByteOut(bl);
		VS_RegWrite(vsadr,bh*256+bl);
		break;
	case 'i': SPI_Speed(0x04); VS_FullReset(); break;
	case 's': VS_Status(); break;
*/
  }
}
//-------------------------------------------------------------------
void MIDI_NoteOn(byte note,byte vol)  {AUART_CharOut(0x90); AUART_CharOut(note); AUART_CharOut(vol);}
void MIDI_NoteOff(byte note)          {AUART_CharOut(0x90); AUART_CharOut(note); AUART_CharOut(0x00);}
//-------------------------------------------------------------------
byte CheckForVolumeChange(void)
{
byte c,res;
res=1;
if (DUART_RDRF())
	{
	c=DUART_CharIn();
	if ((c=='+')||(c=='-'))
		{
		if (c=='+') bvol--; else bvol++;
		VS_RegWrite(0x0B,bvol*256+bvol);
		DUART_TextOut("\n\rAttenuation=0x"); DUART_ByteOut(bvol); DUART_TextOut("  (press '+' or '-' for volume, or any other key to quit)");
		}
	else res=0;
	}
return res;
}
//-------------------------------------------------------------------
void Test_MIDI_realtime(void)
{
byte b,cont,chan;
SPI_SD_SS1; SPI_VS_CS1; SPI_VS_DCS1;
DUART_TextOut("\n\r\n\rTest_MIDI_realtime\n\r");
VS1053_MIDImode; SPI_Speed(0x04); VS_FullReset(); VS_Status();
DUART_TextOut("\n\rPlaying notes...");
bvol=0x05; DUART_TextOut("\n\rAttenuation=0x"); DUART_ByteOut(bvol); DUART_TextOut("  (press '+' or '-' for volume, or any other key to quit)");
b=0x39; cont=1; chan=0;
while (cont)
  {
	MIDI_NoteOn(b,0x7F); PIT_Delay100us(1000); MIDI_NoteOff(b);
	b++; if (b>0x39+12) {b=0x39; chan++; if (chan>0x7F) chan=0; AUART_CharOut(0xC0); AUART_CharOut(chan); DUART_TextOut("\n\rMIDI Channel 0x"); DUART_ByteOut(chan);}
  cont=CheckForVolumeChange();
  }
}
//-------------------------------------------------------------------
void Test_Music_files(void)
{
byte key;
SPI_SD_SS1; SPI_VS_CS1; SPI_VS_DCS1;
DUART_TextOut("\n\r\n\rTest_Music_files\n\r");
VS1053_MP3mode; SPI_Speed(0x04); VS_FullReset(); VS_Status();
DUART_TextOut("\n\r");
sderror=SD_CardReset(); DUART_ByteOut(sderror);
if ((sderror==0)&&(songs>0))
  {
	DUART_TextOut("\n\rPick a song 'a' to '"); DUART_CharOut('a'+songs-1); DUART_TextOut("': ");
	do key=DUART_CharIn(); while ((key<'a')||(key>'a'+songs-1));
	SD_PlayFile(key-'a');
  }
}
//-------------------------------------------------------------------
void SD_PlayFile(byte f)
{
byte c,cont;
word w;
dwrd d,l,bytes;
d=songentry[f];
l=songlen[f];
bytes=0;
DUART_TextOut("\n\rPlaying song '"); DUART_CharOut('a'+f); DUART_TextOut("'  len=0x"); DUART_DWrdOut(l); DUART_TextOut("  entry=0x"); DUART_DWrdOut(d);
bvol=0x05; DUART_TextOut("\n\rAttenuation=0x"); DUART_ByteOut(bvol); DUART_TextOut("  (press '+' or '-' for volume, or any other key to quit)");
cont=1;
while ((bytes<l)&&(cont))
	{
	SD_ReadBlock(d++);
	SPI_Speed(0x00);
	for (w=0;w<512;w+=32) VS_Play32B(w);
	bytes+=512;
	cont=CheckForVolumeChange();
	}
}
//-------------------------------------------------------------------
void SD_Analyze(void)
{
byte nfa,spc,file,first,letter;
word rvs,ofs;
dwrd lba,spf,dir,len,fcl;
DUART_TextOut("\n\r\n\rSD_Analyze\n\r");
sderror=SD_CardReset(); DUART_ByteOut(sderror);
DUART_TextOut("\n\r");
if (sderror==0)
  {
	SD_ReadBlock(0); DUART_TextOut("Boot Signature=0x"); DUART_ByteOut(SDin[0x1FE]); DUART_ByteOut(SDin[0x1FF]); DUART_TextOut("\n\r");
	lba=SDin[0x1C6]+SDin[0x1C7]*256+SDin[0x1C8]*256*256+SDin[0x1C9]*256*256*256; DUART_TextOut("LBA=0x"); DUART_DWrdOut(lba); DUART_TextOut("\n\r");
	SD_ReadBlock(lba);
	spc=SDin[0x0D]; DUART_TextOut("Sectors per Cluster=0x"); DUART_ByteOut(spc); DUART_TextOut("\n\r");
	rvs=SDin[0x0E]+SDin[0x0F]*256; DUART_TextOut("Reserved Sectors=0x"); DUART_WordOut(rvs); DUART_TextOut("\n\r");
	nfa=SDin[0x10]; DUART_TextOut("Number of FATs=0x"); DUART_ByteOut(nfa); DUART_TextOut("\n\r");
	spf=SDin[0x24]+SDin[0x25]*256+SDin[0x26]*256*256+SDin[0x27]*256*256*256; DUART_TextOut("Sectors per FAT=0x"); DUART_DWrdOut(spf); DUART_TextOut("\n\r");
  dir=lba+rvs+nfa*spf; DUART_TextOut("Directory=0x"); DUART_DWrdOut(dir); DUART_TextOut("\n\r");
  SD_ReadBlock(dir);
  songs=0;
  for (file=0;file<maxsongs;file++)
    {
  	ofs=file*32;
  	first=SDin[ofs];
  	len=SDin[ofs+0x1C]+SDin[ofs+0x1D]*256+SDin[ofs+0x1E]*256*256+SDin[ofs+0x1F]*256*256*256;
  	if ((first>' ')&&(first<'z')&&(len>5000))
  	  {
  		DUART_TextOut("---> Song '"); DUART_CharOut('a'+songs); DUART_TextOut("' = [");
  		for (letter=ofs+0x00;letter<ofs+0x07;letter++) DUART_CharOut(SDin[letter]);
  		DUART_CharOut('.');
  		for (letter=ofs+0x08;letter<ofs+0x0B;letter++) DUART_CharOut(SDin[letter]);
  		fcl=SDin[ofs+0x1A]+SDin[ofs+0x1B]*256+SDin[ofs+0x14]*256*256+SDin[ofs+0x15]*256*256*256;
  		DUART_TextOut("]  len=0x"); DUART_DWrdOut(len); DUART_TextOut("  clusters=0x"); DUART_DWrdOut(fcl);
  		DUART_TextOut("\n\r");
  		songlen[songs]=len;
  		songentry[songs]=dir+(fcl-2)*spc;
  		songs++;
  	  }
    }
  }
}
//-------------------------------------------------------------------
void Test_Microphone(void)
{
byte cont;
word adc,iw;
DUART_TextOut("\n\r\n\rTest_Microphone\n\r");
DUART_TextOut("\n\r   ---> Recording (1s)...");
for (iw=0;iw<nsamples;iw++)
  {
	while (!PIT_TFLG1); PIT_TFLG1=1;
	adc=ADC_Read(7);
	m[iw]=adc;
  }
cont=1;
while (cont)
  {
	DUART_TextOut("\n\r   ---> Playback (1s)...");
	for (iw=0;iw<nsamples;iw++)
		{
		while (!PIT_TFLG1); PIT_TFLG1=1;
		DAC0_DAT0H=0x00; DAC0_DAT0L=m[iw];
		}
	if (DUART_RDRF()) cont=0;
  }
}
//-------------------------------------------------------------------
void ADC_Init(void)
{
SIM_SCGC6|=SIM_SCGC6_ADC0_MASK;              // Enable ADC module
ADC0_CFG2=0x10;                              // Long sample time, ADCxxB channels selected
ADC0_CFG1=0x40;                              // mode 8-bit(0x40), 10-bit(0x48), 12-bit(0x44), busclk/4 = 5.24MHz
ADC0_SC2=0x00;                               // DMA disabled, VREH & VREF pins selected
ADC0_SC3=0x00;                               // Single Conversion, Continuous Converstion is (0x08)
PORTD_PCR6=PORT_PCR_MUX(0);                  // Set Pin D6 to ADC0_SE7b function
}
//---------------------------------------------------------------------
word ADC_Read(int chan)                      // Analog-to-Digital Converter byte read: enter channel to read
{ADC0_SC1A=chan; while (!(ADC0_SC1A&ADC_SC1_COCO_MASK)); return (word)ADC0_RA;}
//-------------------------------------------------------------------
void DAC_Init(void)
{
SIM_SCGC6|=SIM_SCGC6_DAC0_MASK;              // Enable DAC0 module
DAC0_SR=0x02;                                // DAC pointer is 0
DAC0_C0=0xC0;                                // DAC0 Enabled, VDDA is DAC0 ref voltage
DAC0_C1=0x00;                                // No DMA, Buffer read pointer disabled
PORTE_PCR30=PORT_PCR_MUX(0);                 // E30=DAC0
}
//-------------------------------------------------------------------
void Test_DAC(void)
{

byte cont;
word adc,iw;
DUART_TextOut("\n\r\n\rTest_DAC\n\r");
DUART_TextOut("\n\r   ---> Populating sine waveform...");
for (iw=0;iw<nsamples;iw++)	m[iw]=128+127*sin(iw*6.283f*440/nsamples);
cont=1;
while (cont)
  {
	DUART_TextOut("\n\r   ---> Playing sine wave...");
	for (iw=0;iw<nsamples;iw++)
		{
		while (!PIT_TFLG1); PIT_TFLG1=1;
		DAC0_DAT0H=0x00; DAC0_DAT0L=m[iw];
		}
	if (DUART_RDRF()) cont=0;
  }
}
