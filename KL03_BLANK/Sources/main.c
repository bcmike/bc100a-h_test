// INCLUDES /////////////////////////////////////////////////////////
//-------------------------------------------------------------------
#include "MKL03Z4.h"                     // KE03 M0+ register definitions

// DEFINES //////////////////////////////////////////////////////////
//-------------------------------------------------------------------
#define Overflow1    (SysTick->CTRL&SysTick_CTRL_COUNTFLAG_Msk)  // used by MCU_Delay()

// GLOBAL CONSTANTS /////////////////////////////////////////////////
//-------------------------------------------------------------------

// GLOBAL VARIABLES /////////////////////////////////////////////////
//-------------------------------------------------------------------

// FUNCTION HEADERS /////////////////////////////////////////////////
//-------------------------------------------------------------------
void MCU_Init(void);                         // initializes MCU for Freedom Board
void MCU_Delay(long delay);                  // delay in multiples of 1ms
//---------------------------------------------------------------------
void RGB(int RedL,int GreenL,int BlueL,int RedR,int GreenR,int BlueR);

// *** MAIN *********************************************************
//-------------------------------------------------------------------
int main(void)
{
MCU_Init();                                  // MCU Initialization; has to be done prior to anything else
RGB(0,0,0,0,0,0);

for(;;)                                      // forever loop
  {
	MCU_Delay(1000);   //works fine
	RGB(1,0,0,0,0,0);
	MCU_Delay(1000);
	RGB(0,0,0,0,0,0);

	MCU_Delay(1000);   //works fine
	RGB(0,1,0,0,0,0);
	MCU_Delay(1000);
	RGB(0,0,0,0,0,0);

	MCU_Delay(1000);   //works fine
    RGB(0,0,1,0,0,0);
    MCU_Delay(1000);
    RGB(0,0,0,0,0,0);

    //RGB(0,0,0,1,0,0);  //not work, no LED at all
    //MCU_Delay(1000);
    //RGB(0,0,0,0,0,0);
    //MCU_Delay(1000);

    //RGB(0,0,0,0,1,0);  //not work, no led at all
    //MCU_Delay(1000);
    //RGB(0,0,0,0,0,0);
    //MCU_Delay(1000);

    MCU_Delay(1000);   //works fine
    RGB(0,0,0,0,0,1);
    MCU_Delay(1000);
    RGB(0,0,0,0,0,0);

  }
// return 0;
}
//-------------------------------------------------------------------
// ******************************************************************


// FUNCTION BODIES //////////////////////////////////////////////////
//-------------------------------------------------------------------
void MCU_Init(void)
{
// Disable Interrupts
__asm("CPSID i");                            // Disable interrupts

// System Registers
SIM->COPC=0x00;                              // Disable COP watchdog
SIM->SCGC5|=SIM_SCGC5_PORTA_MASK;            // Enable clock gate for PORTA registers
SIM->SCGC5|=SIM_SCGC5_PORTB_MASK;            // Enable clock gate for PORTB registers

// System Tick Init
SysTick->CTRL=0;                             // Disable the SysTick Timer
SysTick->LOAD=24000;                         // Busclk/2=LIRC 24MHz, Period=1ms/(1/24000000Hz)=24000
SysTick->VAL=0;                              // Clear the current counter value to 0
SysTick->CTRL=(SysTick_CTRL_ENABLE_Msk
             |SysTick_CTRL_CLKSOURCE_Msk);   // Enable SYS TICK timer and set clock source to processor clock (core clock)

// System clock initialization
MCG->MC|=MCG_MC_HIRCEN_MASK;                 // Enable 48MHz HIRC
MCG->C1=MCG_C1_CLKS(0);                      // Enable Main Clock Source of the 48MHz HIRC

// GPIO Init
PORTB->PCR[1] = PORT_PCR_MUX(1);             // Set pin MUX for GPIO on PORTB pin 1
PORTB->PCR[2] = PORT_PCR_MUX(1);             // Set pin MUX for GPIO on PORTB pin 2
PORTB->PCR[3] = PORT_PCR_MUX(1);             // Set pin MUX for GPIO on PORTB pin 3
PORTB->PCR[4] = PORT_PCR_MUX(1);             // Set pin MUX for GPIO on PORTB pin 4
PORTA->PCR[3] = PORT_PCR_MUX(1);             // Set pin MUX for GPIO on PORTA pin 3
PORTA->PCR[4] = PORT_PCR_MUX(1);             // Set pin MUX for GPIO on PORTA pin 4

PTB->PDDR|=(1<<1);                           // Set pin as output for B1, Blue Right
PTB->PDDR|=(1<<2);                           // Set pin as output for B2, Red Left
PTB->PDDR|=(1<<3);                           // Set pin as output for B3. Red Right, Open Drain
PTB->PDDR|=(1<<4);                           // Set pin as output for B4, Green Right, Open Drain
PTA->PDDR|=(1<<3);                           // Set pin as output for A3, Green Left
PTA->PDDR|=(1<<4);                           // Set pin as output for A4, Blue Left

}
//---------------------------------------------------------------------
void MCU_Delay (long delay)                  // Delay in multiples of 1ms (e.g. use 1000 for 1 second)
{long delw; for (delw=0;delw<delay;delw++) {while (!Overflow1);}}
//---------------------------------------------------------------------
void RGB(int RedL,int GreenL,int BlueL,int RedR,int GreenR,int BlueR)  // RGB-LED Control: 1=on, 0=off, for each of the 3 colors
{
if (RedL   ==1) PTB->PSOR|=(1<<2);  else PTB->PCOR|=(1<<2);
if (GreenL ==1) PTA->PSOR|=(1<<3);  else PTA->PCOR|=(1<<3);
if (BlueL  ==1) PTA->PSOR|=(1<<4);  else PTA->PCOR|=(1<<4);
if (RedR   ==1) PTB->PSOR|=(1<<3);  else PTB->PCOR|=(1<<3);
if (GreenR ==1) PTB->PSOR|=(1<<4);  else PTB->PCOR|=(1<<4);
if (BlueR  ==1) PTB->PSOR|=(1<<1);  else PTB->PCOR|=(1<<1);
}
//---------------------------------------------------------------------
