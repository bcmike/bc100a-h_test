// INCLUDES /////////////////////////////////////////////////////////                  
//-------------------------------------------------------------------
#include "spi.h"


// FUNCTION BODIES //////////////////////////////////////////////////                    
//-------------------------------------------------------------------
void SPI_Init(byte speed)
{
SPI_SCG; SPI_MUX; SPI_GPIO_outputs;
SPI0->C1=0x5C;                                              // no interrupts, master, CPOL=CPHA=1, SS*=GPIO, msb first
SPI0->C2=0x00;                                              // standard
SPI0->BR=speed;                                             // SPI=bus/(2^(n+1)) (0x01:6MHz, 0x04:750kHz)
(void)SPI0_S;
(void)SPI0_DL;
}
//-------------------------------------------------------------------
void SPI_Speed(byte speed)
{SPI0->BR=speed;}                                           // SPI=bus/(2^(n+1)) (0x01:6MHz, 0x04:750kHz)
//-------------------------------------------------------------------
byte SPI_CharShift(byte data)
{
while ((SPI0->S&0x20)==0);
SPI0->DL=data;
while ((SPI0->S&0x80)==0);
return SPI0->DL;
}
//-------------------------------------------------------------------
