#ifndef __COMMON_H
#define __COMMON_H


// HARDWARE /////////////////////////////////////////////////////////
//-------------------------------------------------------------------
// B18 = nRGB_R
// B19 = nRGB_G
// D01 = nRGB_B


// INCLUDES /////////////////////////////////////////////////////////                 
//-------------------------------------------------------------------
#include "MKL27Z4.h"


// TYPEDEFS /////////////////////////////////////////////////////////
//-------------------------------------------------------------------
typedef unsigned char	      byte;
typedef unsigned short int	  word;
typedef unsigned long int	  dwrd;
typedef char			      int8;
typedef short int	          int16;
typedef int		              int32;


// DEFINES //////////////////////////////////////////////////////////                    
//-------------------------------------------------------------------
#define RGB_SCG                   SIM_SCGC5|=SIM_SCGC5_PORTB_MASK; SIM_SCGC5|=SIM_SCGC5_PORTD_MASK
#define RGB_MUX                   PORTB_PCR18=PORT_PCR_MUX(1); PORTB_PCR19=PORT_PCR_MUX(1); PORTD_PCR1 =PORT_PCR_MUX(1)
#define RGB_GPIO_outputs          GPIOB_PDDR|=(1<<18); GPIOB_PDDR|=(1<<19); GPIOD_PDDR|=(1<< 1)
#define RGB_R0                    GPIOB_PSOR=(1<<18)
#define RGB_R1                    GPIOB_PCOR=(1<<18)
#define RGB_G0                    GPIOB_PSOR=(1<<19)
#define RGB_G1                    GPIOB_PCOR=(1<<19)
#define RGB_B0                    GPIOD_PSOR=(1<< 1)
#define RGB_B1                    GPIOD_PCOR=(1<< 1)
//...................................................................
#define SPI_SCG                   SIM_SCGC4|=SIM_SCGC4_SPI0_MASK; SIM_SCGC5|=SIM_SCGC5_PORTD_MASK
#define SPI_MUX                   PORTB_PCR0 =PORT_PCR_MUX(1); PORTD_PCR1 =PORT_PCR_MUX(2); PORTD_PCR2 =PORT_PCR_MUX(2); PORTD_PCR3 =PORT_PCR_MUX(2)
#define SPI_GPIO_outputs          GPIOB_PDDR=0x01; GPIOB_PSOR=0x01  //GPIOD_PDDR|=(1<< 0); GPIOD_PSOR=(1<< 0)
#define SPI_SS0                   GPIOB_PCOR=0x01 //GPIOD_PCOR=(1<< 0)
#define SPI_SS1                   GPIOB_PSOR=0x01 //GPIOD_PSOR=(1<< 0)
//...................................................................
#define DUART_Init                LPUART0_Init
#define DUART_CharOut             LPUART0_CharOut
#define DUART_NibbOut             LPUART0_NibbOut
#define DUART_ByteOut             LPUART0_ByteOut
#define DUART_WordOut             LPUART0_WordOut
#define DUART_DWrdOut             LPUART0_DWrdOut
#define DUART_TextOut             LPUART0_TextOut
#define DUART_RDRF                LPUART0_RDRF
#define DUART_CharIn              LPUART0_CharIn
#define DUART_NibbIn              LPUART0_NibbIn
#define DUART_ByteIn              LPUART0_ByteIn
#define DUART_DbleOut             LPUART0_DbleOut

#define LPUART0_SCG               SIM_SCGC5|=SIM_SCGC5_LPUART0_MASK; SIM_SCGC5|=SIM_SCGC5_PORTA_MASK; SIM_SOPT2|=SIM_SOPT2_LPUART0SRC(1);
#define LPUART0_MUX               PORTA_PCR1 =PORT_PCR_MUX(2); PORTA_PCR2 =PORT_PCR_MUX(2)
//...................................................................
#define LPUART1_SCG               SIM_SCGC5|=SIM_SCGC5_LPUART1_MASK; SIM_SCGC5|=SIM_SCGC5_PORTE_MASK; SIM_SOPT2|=SIM_SOPT2_LPUART1SRC(1);
#define LPUART1_MUX               PORTE_PCR0 =PORT_PCR_MUX(3); PORTE_PCR1 =PORT_PCR_MUX(3)


// GLOBAL VARIABLES /////////////////////////////////////////////////                    
//-------------------------------------------------------------------                    


// FUNCTION HEADERS /////////////////////////////////////////////////                    
//-------------------------------------------------------------------
void MCU_Init(void);
void PIT_Init(void);
void PIT_Delay(dwrd delay);                                          // delay in multiples of 100us
void RGB_Init(void);
void RGB(byte R,byte G,byte B);


#endif //__COMMON_H
